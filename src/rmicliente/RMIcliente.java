/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmicliente;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import rmi.RMIinterface;

/**
 *
 * @author EstebanRM
 */
public class RMIcliente {

    public static void main(String[] args) {
        RMIcliente cliente = new RMIcliente();
        
    }
    
    public boolean connectServer(String Cedula){
        try{
            Registry  registro = LocateRegistry.getRegistry("127.0.0.1",7777);
            RMIinterface interfaz = (RMIinterface)registro.lookup("RMIremoto");            
            boolean Bandera;
            Bandera = interfaz.validarCedula(Cedula);
            System.out.println("La cedula es "+Bandera);         
            return Bandera;
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public String connectServer2(String Cedula){
        try{
            Registry  registro = LocateRegistry.getRegistry("127.0.0.1",7777);            
            RMIinterface interfaz2 = (RMIinterface)registro.lookup("RMIremoto2");
            String usuario;
            usuario = interfaz2.encontrarCedula(Cedula);
            System.out.println("La cedula es "+usuario);         
            return usuario;
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return "No se encontro";
        }
    }
    
}
